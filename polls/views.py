from django.http import HttpResponse
from django.http import Http404
#from django.template import loader

from django.shortcuts import render, get_object_or_404

from .models import Question

def index(request):
	latest_questions_list= Question.objects.order_by('pub_date')[:5]
	#output= ','.join([q.question_text for q in latest_questions_list])
	# --template= loader.get_template('polls/index.html')
	context= {
		'latest_questions_list': latest_questions_list
	}
	#-- return HttpResponse(template.render(context, request))
	return render(request, 'polls/index.html', context)

def detail(request, question_id):
	# try:
	# 	question= Question.objects.get(pk=question_id)
	# except Question.DoesNotExist:
	# 	raise Http404("Question does not exist")
	question= get_object_or_404(Question, pk=question_id)
	return render(request, 'polls/detail.html', {'question': question})


def results(request, question_id):
	response= "You are looking at results of question %s."
	return HttpResponse( response % question_id)


def vote(request, question_id):
	return HttpResponse("You are voting at question %s."  %question_id)